#!/usr/bin/env Rscript

library(tidyverse)
library(bigrquery)
library(DBI)



# upload a valid service account key and use this
service_account_file_path <- '../../resources/service-account.json'
gcp_project_id <- jsonlite::read_json(
        '../../resources/service-account.json'
    )$project_id
bigrquery::bq_auth(path = service_account_file_path)



# open a connection to the world bank population health data
# https://console.cloud.google.com/marketplace/details/the-world-bank/global-health?filter=solution-type:dataset&filter=category:health&q=public%20data&id=f3c38e10-2c45-43c8-8a12-8d749ba987ee
con <- dbConnect(
    bigrquery::bigquery(),
    project = "bigquery-public-data",
    dataset = "world_bank_health_population",
    billing = gcp_project_id
)



# query world bank data from google cloud
tbl_smoking_prevalence <- tbl(con, "health_nutrition_population") %>%
    filter(
        indicator_name %in% c(
            "Smoking prevalence, females (% of adults)",
            "Smoking prevalence, males (% of adults)",
            "Smoking prevalence, total, ages 15+"
        )
    ) %>%
    mutate(gender = str_extract(indicator_name, "female|male|total")) %>%
    filter(gender != 'total') %>%
    select(-indicator_name, -indicator_code) %>%
    arrange(country_code, year, gender)

# nothing happend so far, everything can be converted to SQL
show_query(tbl_smoking_prevalence)

# fetch first 3 entries
print(tbl_smoking_prevalence, n = 3)

# download the entire thing and save locally
tbl_smoking_prevalence <- collect(tbl_smoking_prevalence)
print(tbl_smoking_prevalence)



# analyse United Kingdom alone
tbl_smoking_prevalence %>%
    filter(country_code == 'GBR') %>%
    ggplot(aes(year, value, color = gender)) +
        geom_point() +
        geom_line() +
        ylim(c(0, NA))

tbl_smoking_prevalence %>%
    filter(country_code == 'GBR') %>%
    lm(value ~ year*gender, data = .) %>%
    summary()


# map analysis over all countries (with data for both genders)
tbl_smoking_models <- tbl_smoking_prevalence %>%
    group_by(country_name) %>%
    filter(gender %>% unique %>% length == 2) %>%
    nest() %>%
    mutate(
        model = map(data, ~lm(value ~ year*gender, data = .))
    ) %>%
    ungroup()

# show overall model stats
tbl_smoking_models %>%
    mutate(
        fit = map(model, broom::glance)
    ) %>%
    unnest(fit)


# plot the worst fitting modeles (by R^2)
tbl_smoking_models %>%
    # glance computes overall model statistics like R^2,
    # get the 8 models with worst fit
    mutate(fit = map(model, broom::glance)) %>%
    unnest(fit) %>%
    arrange(r.squared) %>%
    head(n = 8) %>%
    select(country_name, data, model) %>%
    mutate(augmented = map(model, broom::augment)) %>%
    unnest(augmented) %>%
    ggplot(aes(year, value, color = gender)) +
        geom_point() +
        geom_line(aes(y = .fitted)) +
        ylim(c(0, 100)) +
        facet_wrap(~country_name, nrow = 2) +
        theme(
            axis.text.x = element_text(angle = 60, hjust = 1)
        )

# plot the models with biggest interaction term
tbl_smoking_models %>%
    # extract 8 models with highest interaction term
    mutate(parameters = map(model, broom::tidy)) %>%
    unnest(parameters) %>%
    filter(term == 'year:gendermale') %>%
    arrange(desc(abs(estimate))) %>%
    head(n = 8) %>%
    # 'augment' original data with fitted values, standard error etc
    select(country_name, data, model) %>%
    mutate(augmented = map(model, broom::augment)) %>%
    unnest(augmented) %>%
    # plot data + fitted values
    ggplot(aes(year, value, color = gender)) +
        geom_point() +
        geom_line(aes(y = .fitted)) + # .fitted is genrated by
        # 'broom::augment'
        ylim(c(0, 100)) +
        facet_wrap(~country_name, nrow = 2) +
        theme(
            axis.text.x = element_text(angle = 60, hjust = 1)
        )
