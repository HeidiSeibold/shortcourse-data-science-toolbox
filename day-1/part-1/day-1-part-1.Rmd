---
title:    "Working with Data, Plotting"
subtitle: "<small>*Data Science Toolbox, Day 1*</small>"
author:   "<small>@kevin_kunzmann</small>"
date:     "`r Sys.Date()`"
output: 
    revealjs::revealjs_presentation:
        css:            '../../resources/styles.css'
        self_contained: false
        reveal_plugins: ["notes"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(
    echo   = TRUE, 
    prompt  = TRUE, 
    comment = NA
)

library(tidyverse)
```



# Outline

**Before lunch**

* what is the `tidyverse` and why should I care?
* data import with `readr`
* data 'wrangling' with `dplyr` + `tidyr`
* systematic plotting with `ggplot2`

**After Lunch**

* connecting to a proper database (Google BigQuery)
* querying generic RESTful APIs
* functional programming with `purrr`
* projects start



## Communication

* (hopefully) challenging course content!

* never think you or your question is stupid, please ask!

* be respectful of others questions/problems and try to help!

* tell me when you feel that I am not addressing your **or your peers** questions appropriately!

* I can adjust the speed - if you tell me!

* there will be plenty of opportunities in the breaks/after the course to
ask questions - do not wait to ask if it is critical to your understanding though!



## Who are we?

<iframe src="https://hackmd.io/Z5_0SK5yQmihhUBBJqvJyA?view" width="1000px" height="550px"></iframe> 

<small> go to https://bit.ly/342DSLz and hack away! </small>


## <img src="../../resources/figures/iceberg.jpg" height="600px">

<aside class="notes">

* We will only cover the tip of the iceberg, by no means exhaustive!
* Dig deeper!

</aside>



## Let's get you geared up to dive deeper!

<img src="../../resources/figures/diver.jpg" height="550px">

<aside class="notes">

* but you'll have the gear to dive deeper

</aside>



## Concept

* fast-moving field, impossible to teach everything

* problem-oriented learning

* peer learning 

* steep learning curve!

* technology/software change, problems not so quickly!

* R-centered: more common than Python in medical stats

> *Nobody expects you to understand/master everything we cover - just get interested and know where to continue digging!*



## Materials

* avoid issues with setting up your computing environments

* simply visit **https://bit.ly/2U9rvJj** and enjoy your own cloud instance
with everything pre-installed

* thanks to **https://mybinder.org/** and sponsors!

* beware that **nothing is saved** once you close the browser!

* **download results** locally before closing



## <iframe src="https://mybinder.org/v2/gl/kkmann%2Fshortcourse-data-science-toolbox/master?urlpath=rstudio" width="1200px" height="600px"></iframe> 

<small> trick: invoke `watch -n 10 echo 'still here'` to keep the instance alive for
up to 7 hours </small>



## Materials - what if I know `git` already?

* good for you!

* just `git clone` and you have everything
```{bash, eval=FALSE}
git clone https://gitlab.com/kkmann/shortcourse-data-science-toolbox
cd shortcourse-data-science-toolbox
Rscript install.R # or check manually for required dependencies
```

* the repository contains the entire material and the sources for building 
the slides

* `makefile` supported (requires Unix system)











# Welcome to the `tidyverse`

<img src="../../resources/figures/tidyverse.png" style="height:500px;">



## What is the `tidyverse`?

* collection of integrated, opinionated
R packages

* essentially an **R dialect**

* <p> genius who started it all: 
<img src="../../resources/figures/hadley.jpg" style="height:250px;" align="right">
**Hadley Wickham**, CTO RStudio (+ 100s of OS contributors!) </p>

* philosophy: **get 90% of the work done in 10% of the time**

* read: [**R for Data Science**](https://r4ds.had.co.nz/)



## Statistics vs. Data Science

> “[...] What I do is fundamentally statistics. The fact that data science 
exists as a field is a colossal failure of statistics. To me, that is what 
statistics is all about. It is gaining insight from data using modelling and
visualization. Data munging and manipulation is hard and statistics has just 
said that’s not our domain.” - **Hadley Wickham, CTO R Studio**



<aside class="notes">

* excellent support since OS!
* consequent OS strategy by RS
* HW arguably huge impact on science over the last years

</aside>



## Forget (almost) everything you know about R!

<img src="../../resources/figures/dont_panic.jpg" style="height:50%;">




# File import/export

* pletora of **different file formats** out there

* most **commonly** seen
    * `.csv` 
    * `.xlsx`
    * SAS, Stata, SPSS
    * `.json`, `.yml`, ... 
    
* need tools to **load data into R**!

## The workhorse: `.csv`

* <p> <img src="../../resources/hexstickers/readr.png" align="right" width="200px;"> `readr` for **fast, stable, consistent** rectangular text file I/O </p>
* beware of defaults: `.csv` is not a well-defined format, **be specific about what to expect** to get errors!

```{r, eval=FALSE}
col_types <- cols(
    .default = col_double(),
    name     = col_character()
)
tbl_data <- read_csv(
    'tbl_data.csv',
    col_types = col_types
)
write_csv(tbl_data, 'tbl_data2.csv')
```

<aside class="notes">

* funny that .csv is still pretty much THE universal file format - 
although it is not even well-defined!

</aside>


## Blank horror: `.xlsx`

* <p> <img src="../../resources/hexstickers/readxl.png" align="right" width="200px;"> `readxl` not loaded by default with `tidyverse`, prefix with `readxl::` </p>
* beware of formulas etc. lurking in Excel files!
* avoid Excel input at all cost!

```{r, eval=FALSE}
tbl_data <- readxl::read_excel(
    'tbl_data.xlsx', 
    sheet     = 1, 
    range     = 'B3:D87', 
    guess_max = 10^4
)
```



## The Competition: SPSS, Stata, SAS

* <p> <img src="../../resources/hexstickers/haven.png" align="right" width="200px;"> `haven` for reading and
writing SPSS, SAS, Stata </p>
* limited support for more advanced features like SAS catalogue files etc.

```{r, eval=FALSE}
tbl_data <- haven::read_sas('tbl_data.sas7bdat')
haven::write_sas(tb_data, 'tbl_data2.sas7bdat')
```



## The Generalist: `.json` (1)

* querying data from the web often returns `.json` files

* stupid key-value or key-list pairs (list-of-lists)

* very flexible

* often used as configuarion file
```
{
    "name": "John", 
    "phoneNumbers": [
        {"type": "home", "number": "212 555-1234"},
        {"type": "office", "number": "646 555-4567"}
    ]
}
```



## The Generalist: `.json` (2)

* parsed into lists-of-lists

* bit tedious to unpack (more later!)

```{r}
jsonlite::read_json('my_json_data.json')
```



# What the heck is a `tibble`?

<img src="../../resources/hexstickers/tibble.png" height="500px;">

<aside class="notes">

* Kiwi way of pronouncing 'tbl' - originally 'tibbles' where 'tbl_df', i.e., 'tibble diff'...
* Texas Instruments Ti-33
* Star Trek 'Tribble' 'Tribbles look cute initially, but then they multiply, take over EVERYTHING and generally cause trouble.' (return of the tribbles) + font and stars

</aside>



## A `tibble` is ...

* <p> <img src="../../resources/hexstickers/tibble.png" align="right" width="200px;"> ... a modern version of the `data.frame` </p>

* ... the base class for tabular data in the `tidyverse`

* ... a funny way of pronouncing 'table'!



##  **Type safety**: `tibble` vs. `data.frame`

```{r, collapse=FALSE}
data.frame(a = "a")$a
tibble(a = "a")$a
```

## **Access safety**: `tibble` vs. `data.frame`

```{r, collapse=FALSE}
data.frame(a = "a")$b
tibble(a = "a")$b
```

## A `data.frame` cannot store cool content ...

```{r, collapse=FALSE}
fits <- list(
    lm(y ~ x, data = list(x = c(0, 1), y = c(0, 1))),
    glm(y ~ x, data = list(x = c(0, 1), y = c(0, 1)))
)

tryCatch(
    data.frame(a = fits), 
    error = function(e) cat(e$message)
)
```

## ... but a `tibble` can!

```{r}
tibble(a = fits)
```
```{r}
tibble(a = fits)$a[[1]]
```



#  Let's talk about 'piping' 

<img src="../../resources/figures/piper.jpg">



## 'Piping' = connect processing steps

<img src="../../resources/figures/piping.jpg" height="550px;">



## 'Piping' - Unix has done it for ages...

* essential to Unix command line: feed output of one
command to the next
```{bash}
ls | grep .json | wc -l
```

* in most languages - **reverse order of execution**:
```{r}
length(grep(".json", list.files()))
```

* **unintuitive**, **hard to read/understand**! 

<aside class="notes">

* gets cryptic for more than 2 nested function calls
* completely oposite to the way of thinking

</aside>


## The piping operator '`%>%`' (1)

* bad solution: use **temporary variables** - error prone, ugly!
```{r}
tmp1 <- list.files()
tmp2 <- grep(".json", tmp1)
length(tmp2)
```

* better solution: **piping** with `magrittr::%>%` operator
```{r}
list.files() %>% 
    grep(".json", .) %>% 
    length
```



## The piping operator '`%>%`' (2)

* how does it work?

```
x %>% f := x %>% f() := f(x)$
x %>% f(y) := f(x, y)
x %>% f(y, .) := f(y, .)
```

* can be combined with `{}` for **anonymous functions**:
```{r}
1 %>% {. + .} %>% {.^2}
```

* code is written/read **in order of execution**

* idiomatic throughout entire tidyverse!



# 'Tidy Data'

<img src="../../resources/figures/tidy-data.png">

* each variable is a column
* each observation is a row
* each type of observational unit is a table





## Tidy data

* [Hadley Wickham, 2013](https://www.jstatsoft.org/article/view/v059i10/)

* just sloppy definition of ['third normal form'](https://en.wikipedia.org/wiki/Third_normal_form) for databases

* trivial, but almost never found in practice!

* normal form allows expressive code (format is clear)

* usually takes many steps and great effort to get 'tidy data'

* if you are involved in **data collection**: make sure that your data is **'tidy'**
right **from the beginning**!





## Let's get our hands dirty!

<img src="../../resources/figures/dirtyhands.jpg" style="height:300px;">

* World Bank data - Health Nutrition Population (HNP)
* I messed up an excerpt of the data to serve as example
* let's create a 'pipe' to clean up the mess!



## First look

```{r}
library(tidyverse)

tbl_mess <- haven::read_sas("../../resources/data/messy-data-1.sas7bdat")

tbl_mess
```

## Better: horizontally

```{r}
glimpse(tbl_mess)
```


## 1) fix the `xyz %` problem

```{r}
tmp <- tbl_mess %>% 
    mutate_at(
        vars(risk_poverty_after_surgery_USA, risk_poverty_after_surgery_CHN),
        function(str) {
            str_replace(str, pattern = ' %$', replacement = '') %>% 
                ifelse(. == 'NA', NA_real_, .) %>% 
                as.numeric
        }
    )
glimpse(tmp)
```



## 2) pivot to long format

```{r}
tmp <- tmp %>% 
    pivot_longer(
        -year,
        "indicator_country",
        "value"
    )
tmp
```



## 3) separate indicator name from country 

```{r}
tmp <- tmp %>% 
    separate(
        indicator_country,
        c("indicator_name", "country_code"),
        sep = '_(?=[^_]+$)' # regular expressions rock!
    )
tmp
```



## 4) compute relative undernourishment 

```{r}
tmp <- tmp %>% 
    pivot_wider(
        names_from  = indicator_name,
        values_from = value
    )
glimpse(tmp)
```



## 5) compute relative undernourishment, cont.

```{r}
tmp <- tmp %>% 
    mutate(
        undernourished_rel = undernourished_people_abs / population_abs
    )
glimpse(tmp)
```



## 6) pivot back to long

```{r}
tmp <- tmp %>% 
    pivot_longer(
        -c(year, country_code),
        names_to  = 'indicator_name',
        values_to = 'value'
    )
tmp
```


## [optional] aggregate all data since 2010 (1)

```{r}
tmp %>% 
    filter(
        year >= 2010
    ) %>% 
    group_by(
        country_code, indicator_name
    ) 
```



## [optional] aggregate all data since 2010 (2)

```{r}
tmp %>% 
    filter(
        year >= 2010
    ) %>% 
    group_by(
        country_code, indicator_name
    ) %>%
    summarize(
        mean = mean(value, na.rm = TRUE),
        sd   = sd(value, na.rm = TRUE)
    ) %>% 
    ungroup()
```



## [optional] everything in one go!

```{r}
tbl_mess %>%
    mutate_at(
        # which columns to change
        vars(
            risk_poverty_after_surgery_USA,
            risk_poverty_after_surgery_CHN
        ),
        # function to apply to both of them
        function(str) {
            str_replace(str, pattern = ' %$', replacement = '') %>%
                ifelse(. == 'NA', NA_real_, .) %>% # explicitly convert 'NA' to NA
                as.numeric
        }
    ) %>%
    pivot_longer(
        -year,
        "indicator_country",
        "value"
    ) %>%
    separate(
        indicator_country,
        c("indicator_name", "country_code"),
        sep = '_(?=[^_]+$)' # regular expressions rock!,
                            # check regular expressions chearsheet
    ) %>%
    pivot_wider(
        names_from  = indicator_name,
        values_from = value
    ) %>%
    mutate(
        undernourished_rel = undernourished_people_abs / population_abs
    ) %>%
    pivot_longer(
        -c(year, country_code),
        names_to  = 'indicator_name',
        values_to = 'value'
    ) %>%
    filter(
        year >= 2010
    ) %>%
    group_by(
        country_code, indicator_name
    ) %>%
    summarize(
        mean = mean(value, na.rm = TRUE),
        sd   = sd(value, na.rm = TRUE)
    ) %>%
    ungroup()
```



## I *** love cheat sheets

<img src="../../resources/figures/cheating.jpg">



## Cheat sheets are nerve-savers!

<img src="../../resources/figures/cheatsheet.png" height="500px;">

<small> https://www.rstudio.com/resources/cheatsheets/ </small>





## Exercise (individually, 25 min)

1) open a binder instance https://bit.ly/2U9rvJj (4 min)

2) open `day-1/part-1/exercise-1.R` (1 min)

3) go through the individual steps, look at intermediate output
and try to reproduce the steps outlined in the slideshow (7 min)

4) familiarize yourself with the `Data Transformation` cheat sheet and try to understand  the regular expressions in the script using the 'Regular Expressions' cheat sheet (13 min)



# Data Visualization

<img src="../../resources/hexstickers/ggplot2.png" height="475px;">



## Aren't graphs 'imprecise'?

* visual bandwidth of humans >> reading bandwidth

* good graphics can convey more information, quicker!

* sometimes frowned upon as 'imprecise' (no *p* values...)

* personally: most tables are boring and unnecessary!

* don't forget to sell/present complex results!



## Example - Anscombe's Quartett

```{r, echo=FALSE}
anscombe %>% 
    pivot_longer(everything(), "variable", "value") %>% 
    separate(variable, c("variable", "example_id"), sep = 1) %>% 
    pivot_wider(
        names_from  = variable, 
        values_from = value,
        values_fn   = list(value = list) 
    ) %>% 
    unnest(c(x, y)) %>% 
    ggplot(aes(x, y)) + 
    geom_point() + 
    geom_smooth(method = "lm", fullrange = TRUE) + 
    facet_wrap(~example_id) +
    theme_bw()
```



## Exercise (pairs, 10 min)

* open binder **https://bit.ly/2U9rvJj**

* create an empty R script

* plot Anscombes Quartet in R, no packages allowed!

* hint: data is available as 
```{r}
anscombe
```



## A solution in base R

```{r, fig.width=10, fig.height=2.75}
par(mfrow = c(1, 4))
for (i in 1:4) {
    x   <- anscombe[[sprintf("x%i", i)]]
    y   <- anscombe[[sprintf("y%i", i)]]
    fit <- lm(y ~ x, list(x = x, y = y))
    plot(x, y)
    lines(x, fitted(fit))
}
```



## What's wrong with this?

* **nothing!** Base R solution is perfectly fine, but ...

* very **procedural coding style** (how over what to do)

* lots of intermediate state (error prone!)

* can get 'fiddly' even in the best case scenario...



## Let's do it the tidy way...

```{r}
print(anscombe)
```

* 'anscombe' data is **not in tidy format**!
* column names (x1, y1, ...) are a combination of 'variable' (x/y) and the
number of the example (1...4)!
* **tidy up data first**, then plot!



## Let's do it the tidy way...

* pivot everything into long format

```{r}
anscombe %>% 
    mutate(order = row_number()) %>% 
    pivot_longer(-order, "variable", "value")
```

## Let's do it the tidy way...

* split columns into variable and example number

```{r}
anscombe %>% 
    mutate(order = row_number()) %>% 
    pivot_longer(-order, "variable", "value") %>% 
    separate(
        variable, 
        c("variable", "example"), 
        sep = "(?<=^[x|y])" # regular expression look-ahead!
    )
```


## Let's do it the tidy way...

* pivot wider to get variables x and y back

```{r}
tbl_anscombe <- anscombe %>% 
    mutate(order = row_number()) %>% 
    pivot_longer(-order, "variable", "value") %>% 
    separate(variable, c("variable", "example"), sep = "(?<=^[x|y])") %>% 
    pivot_wider(
        names_from  = variable, 
        values_from = value
    )
print(tbl_anscombe, n = 3)
```



## Enter: ggplot2

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() +
    geom_smooth(method = "lm", fullrange = TRUE) + 
    facet_wrap(~example, nrow = 1) +
    theme_bw()
```



## ggplot2 - A Grammar of Graphics

* `ggplot2` defines an **expressive** grammar of graphics 

* **what over how**!

* initial autor Hadley Wickham, based on Leland Wilkinson's: [*'The Grammar of Graphics'*](https://www.cs.uic.edu/~wilkinson/TheGrammarOfGraphics/GOG.html)

* modular and flexible

* **depends on tidy data input**

* extremely successful adopted in [python](http://ggplot.yhathq.com/), 
[julia](http://gadflyjl.org/stable/)

* encourages clean concepts

<aside class="notes">

* nobody actually read the book ;) 
* '2' because of big refactor
* 'hate it or love it'
* most popular CRAN package by downloads!

</aside>



## Plotting with ``ggplot2` (1)

* `ggplot()` creates an empty plot given data 
* `aes()` binds aesthetics, first two are x and y coordinates

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) 
```



## Plotting with ``ggplot2` (2)

* add point geometry to visualize the x/y coordinates

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() 
```


## Plotting with ``ggplot2` (3)

* add the smooth (linear model)

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() +
    geom_smooth(method = "lm", fullrange = TRUE) 
```


## Plotting with ``ggplot2` (4)

* use `example` variable to create facets 

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() +
    geom_smooth(method = "lm", fullrange = TRUE) + 
    facet_wrap(~example, nrow = 1) 
```

## Plotting with ``ggplot2` (5)

* lots of styling options available

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() +
    geom_smooth(method = "lm", fullrange = TRUE) + 
    facet_wrap(~example, nrow = 1) +
    theme_bw()
```



## Plotting with ``ggplot2` (6)

* not all are a good choice ;)

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() +
    geom_smooth(method = "lm", fullrange = TRUE) + 
    facet_wrap(~example, nrow = 1) +
    theme_dark()
```



## Plotting with ``ggplot2` (7)

* not all are a good choice ;)

```{r, fig.height=3}
ggplot(tbl_anscombe, aes(x, y)) +
    geom_point() +
    geom_smooth(method = "lm", fullrange = TRUE) + 
    facet_wrap(~example, nrow = 1) +
    theme_void()
```



## Anscombe's Quartet revisited (1)

* are the implied linear models really identical?
* what will this plot do?

```{r, eval=FALSE}
ggplot(tbl_anscombe, aes(x, y, color = example)) +
    geom_point() +
    geom_line(
        stat      = "smooth", 
        method    = "lm", 
        alpha     = 0.5, 
        fullrange = TRUE
    )
```

## Anscombe's Quartet revisited (2)

```{r, echo=FALSE}
ggplot(tbl_anscombe, aes(x, y, color = example)) +
    geom_point() +
    geom_line(
        stat      = "smooth", 
        method    = "lm", 
        alpha     = 0.5, 
        fullrange = TRUE
    )
```



## Anscombe's Quartet revisited - boxplots

```{r, fig.height=4}
tbl_anscombe %>% 
    pivot_longer(c(x, y), names_to = 'variable') %>% 
    ggplot(aes(variable, value, color = example)) +
        geom_boxplot()
```



## ggplot2 - Reference

<iframe src="https://ggplot2.tidyverse.org/reference/" width="1200px" height="550px"></iframe> 

<small> https://ggplot2.tidyverse.org/reference/ </small>


# Your Turn! <img src="../../resources/figures/uncle-sam.png">



## Exercise (pairs, 30 min)

1) open binder **https://bit.ly/2U9rvJj** (4 min)

2) open `day-1/part-1/exercise-2.R` (1 min)

3) tidy up loaded dataset (10 min)

4) visually approach the following questions (15 min): 
    1) How do the indicators develop over time for different countries?
    2) Ho do these indicators differ between eurozone and non-eurozone 
    countries if you take the respective last value as approximate cross section
    (boxplot!) ?
    


# Feedback!

<iframe src="https://hackmd.io/U_Fcim_lQemQ8tDKGUkxIA" width="1200px" height="550px"></iframe> 

<small> https://bit.ly/2HBjXda </small>



# Lunch!

