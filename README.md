[![pipeline status](https://gitlab.com/kkmann/shortcourse-data-science-toolbox/badges/master/pipeline.svg)](https://gitlab.com/kkmann/shortcourse-data-science-toolbox/commits/master)
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/gl/kkmann%2Fshortcourse-data-science-toolbox/master?urlpath=rstudio)





# Shortcourse: data science toolbox

A 2.5 days course on essential skills for health/medical data science. 
The course is R-centered and all materials are cloud-ready.



## Prerequisites

**Must have:**

* laptop
* decent internet connection
* up-to-date Chrome (or Firefox)
* GitLab.com account
* shinyapps.io account 
* basic understanding of R (base data types, variables, functions, loading packages)

**Recommended:**

* Knowing: what is a linear model and how do I fit it in R (formula interface, `lm` function)
* Installation and setup of software 
(see [instructions](https://gitlab.com/HeidiSeibold/setup-git-rstudio-gitlab))
    * R 3.4.4 or newer
    * Rstudio desktop (recent version, please update if in doubt)
    * Git
* R packages listed in `install.R` 


**Optional:**

* Ubuntu 18.04+ (consider a virtual machine!)
* Google Cloud Platform free-tier account 
    * requires credit card details but we will not exceed free tier billing!
    * a ['service account'](https://console.cloud.google.com/projectselector2/iam-admin/serviceaccounts?supportedpurview=project&project&folder&organizationId) with admin rights for BigQuery
    * create and download the service account key file (save as 'service-account.json')
    

## Course materials

This repository allows building the slides via `GNU make`. 
To do so, a Google Cloud Platform serive account key file with BigQuery admin
rights is required (cf. above).
Put this file as `service-account.json` in the `resources` folder.
Never share this file with anyone!
Then call
```shell
make
```
in the root directory.

The easiest way to follow along is to open the [Binder link](https://mybinder.org/v2/gl/kkmann%2Fshortcourse-data-science-toolbox/master?urlpath=rstudio).
This will spin up your personal instance of the required computing environment
in the cloud (Rstudio + Ubuntu).
You can upload your `service-account.json` via the file-pane upload button in 
RStudio.



## Syllabus

### Day 1

1. [R for Data Science](https://r4ds.had.co.nz/)
2. 'Tidy Data', [10.18637/jss.v059.i10](https://doi.org/10.18637/jss.v059.i10)
3. Must have [RStudio cheat-sheets](https://www.rstudio.com/resources/cheatsheets):
    * data import
    * data transformation
    * data visualization
    * purrr
4. *Extremely* helpful [RStudio cheat-sheets](https://www.rstudio.com/resources/cheatsheets):
    * forcats
    * lubridate
    * stringr
    * regular expressions
5. SQL cheat sheet http://www.sqltutorial.org/sql-cheat-sheet/
6. What is a REST API? https://www.youtube.com/watch?v=7YcW25PHnAA
7. List of REST APIs https://www.programmableweb.com/apis
8. Handling many models: https://www.youtube.com/watch?v=rz3_FDVt9eg
     
    
### Day 2

### Day 3

* Official Shiny tutorial: https://shiny.rstudio.com/tutorial/
* Programming with dplyr: https://dplyr.tidyverse.org/articles/programming.html
* deploying Shiny apps: https://shinyapps.io, https://mybinder.org
