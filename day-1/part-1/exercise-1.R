#!/usr/bin/env Rscript


# load libraries ===============================================================
library(tidyverse)

# load data in R
tbl_mess <- haven::read_sas('../../resources/data/messy-data-1.sas7bdat')


# look at the raw data =========================================================
head(tbl_mess) # print first few row

glimpse(tbl_mess) # nice print for tibbles


# start pipe ===================================================================
#
# in practice, pipe length should be limited to remain readable, also note
# that you can easily inspect parts of a pipe by inserting the 'identity' function
# to negate trailing '%>%' when sending to console (ctrl + enter)
#
# example:
# 1 %>%
#   sin()
#
# select and run first line, works fine, then delete identity again to continue
# pipe
# 1 %>% identity
#   sin()
#
tbl_mess %>%
    mutate_at(
        # which columns to change
        vars(
            risk_poverty_after_surgery_USA,
            risk_poverty_after_surgery_CHN
        ),
        # function to apply to both of them
        function(str) {
            str_replace(str, pattern = ' %$', replacement = '') %>%
                ifelse(. == 'NA', NA_real_, .) %>% # explicitly convert 'NA' to NA
                as.numeric
        }
    ) %>%
    pivot_longer(
        -year,
        "indicator_country",
        "value"
    ) %>%
    separate(
        indicator_country,
        c("indicator_name", "country_code"),
        sep = '_(?=[^_]+$)' # regular expressions rock!,
                            # check regular expressions chearsheet
    ) %>%
    pivot_wider(
        names_from  = indicator_name,
        values_from = value
    ) %>%
    mutate(
        undernourished_rel = undernourished_people_abs / population_abs
    ) %>%
    pivot_longer(
        -c(year, country_code),
        names_to  = 'indicator_name',
        values_to = 'value'
    ) %>%
    filter(
        year >= 2010
    ) %>%
    group_by(
        country_code, indicator_name
    ) %>%
    group_by(
        country_code, indicator_name
    ) %>%
    summarize(
        mean = mean(value, na.rm = TRUE),
        sd   = sd(value, na.rm = TRUE)
    ) %>%
    ungroup()
