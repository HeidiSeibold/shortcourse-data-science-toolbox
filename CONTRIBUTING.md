# Contributing 

## Use merge requests

To ensure that the master branch can be built at all times, it is protected and
does not allow directly pushing to it. 
Any changes should instead be pushed to a separate feature branch and a pull
request must be opened. 
Currently the number of required reviews is set to 1 and self-approvals are 
allowed for any approvers. 
Please do make sure to wait for the continuous integration run to be okay before
approving a pull request!

